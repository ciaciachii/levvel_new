$(function () {
    $('.js-basic-example').DataTable({
        responsive: true,
        ordering: false,
    });

    //Exportable table
    $('.js-exportable').DataTable({
        dom: 'Bfrtip',
        responsive: true,
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });

});