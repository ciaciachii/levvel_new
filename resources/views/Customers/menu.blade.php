@extends('layouts.sidebar')

@section('style')
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" />
@endsection

@section('section')
<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>
                    TRANSACTION
                    
                </h2>
            </div>
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                INVENTORIES
                            </h2>
                            <ul class="header-dropdown m-r--4">
                               <a href="{{ route('customer.create') }}">
                                    <button class="btn btn-primary btn-sm"><i class="material-icons" style="color:white;">add_box</i></button>
                                </a>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th width="150px">Name</th>
                                            <th>Contact</th>
                                            <th width="130px">Action</th>
                                        </tr>
                                    </thead>
                                    
                                        @foreach($customer as $ex)
                                        <tr>
                                            
                                            <td>{{ $ex->name }}</td>
                                            <td>{{ $ex->contact }}</td>
                                            
                                           
                                            <td>
                                                

                                                <a href="{{ route('customer.edit', $ex->id) }}">
                                                    <button class="btn btn-warning btn-sm" title='Edit data'><i class="material-icons">edit</i></button>
                                                </a>
                                                
                                                
                                                
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
            
        </div>

    </section>


@endsection

@section('script')
<script>
function myFunction() {
    alert("Testing sections!");
}
</script>
    
    <!-- Jquery Core Js -->
    <script src="{{ asset('assets/css/plugins/jquery/jquery.min.js') }}" type="text/javascript"></script>
    <!-- <script src="../../plugins/jquery/jquery.min.js"></script> -->

    <!-- Bootstrap Core Js -->
    <!-- <script src="{{ asset('assets/css/plugins/bootstrap/js/bootstrap.js') }}" type="text/javascript"></script> -->
    <!-- <script src="../../plugins/bootstrap/js/bootstrap.js"></script> -->
   
    <!-- Select Plugin Js -->
    <!-- <script src="../../plugins/bootstrap-select/js/bootstrap-select.js"></script> -->
    <!-- <script src="{{ asset('assets/css/plugins/bootstrap-select/js/bootstrap-select.js') }}" type="text/javascript"></script> -->

    <!-- Slimscroll Plugin Js -->
    <!-- <script src="../../plugins/jquery-slimscroll/jquery.slimscroll.js"></script> -->
    <script src="{{ asset('assets/css/plugins/jquery-slimscroll/jquery.slimscroll.js') }}" type="text/javascript"></script>

    <!-- Waves Effect Plugin Js -->
    <!-- <script src="../../plugins/node-waves/waves.js"></script> -->
    <script src="{{ asset('assets/css/plugins/node-waves/waves.js') }}" type="text/javascript"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('assets/css/plugins/jquery-datatable/jquery.dataTables.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/css/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/css/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/css/plugins/jquery-datatable/extensions/export/jszip.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/css/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/css/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/css/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/css/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}" type="text/javascript"></script>

    <!-- Custom Js -->
    <script src="{{ asset('assets/js/pages/tables/jquery-datatable.js') }}" type="text/javascript"></script>

@endsection