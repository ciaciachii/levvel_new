@extends('layouts.sidebar')

@section('style')
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" />
@endsection

@section('section')
<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>
                    CUSTOMERS
                    
                </h2>
            </div>
            
            <!-- Advanced Validation -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>EDIT CUSTOMER</h2>
                            
                        </div>
                        <div class="body">
                            <form action="{{ route('customer.update',$id) }}" enctype="multipart/form-data" method="post"> 
                                <input name="_method" type="hidden" value="PATCH">
                                {{ csrf_field() }}
                                

                                
                                
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="name" id="name"  minlength="5" maxlength="191" value="{{ $name }}" required>
                                        <label class="form-label">Name</label>
                                    </div>
                                    <div class="help-info">Min. 5 characters</div>
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span><br>
                                    @endif
                                </div>

                                

                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="contact" id="contact"  minlength="5" maxlength="191" value="{{ $contact }}" required>
                                        <label class="form-label">Added by</label>
                                    </div>
                                    <div class="help-info">Min. 5 characters</div>
                                    @if ($errors->has('contact'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('contact') }}</strong>
                                        </span><br>
                                    @endif
                                </div>
                                
                                
                                
                                <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Advanced Validation -->
            
        </div>
    </section>

@endsection

@section('script')

   <!-- Jquery Core Js -->
    <script src="{{ asset('assets/css/plugins/jquery/jquery.min.js') }}" type="text/javascript"></script>
    <!-- Bootstrap Core Js -->
    <!-- <script src="{{ asset('assets/css/plugins/bootstrap/js/bootstrap.js') }}" type="text/javascript"></script> -->
    <!-- Select Plugin Js -->
    <!-- <script src="../../plugins/bootstrap-select/js/bootstrap-select.js"></script> -->
    <script src="{{ asset('assets/css/plugins/bootstrap-select/js/bootstrap-select.js') }}" type="text/javascript"></script>

    <!-- Slimscroll Plugin Js -->
    <!-- <script src="../../plugins/jquery-slimscroll/jquery.slimscroll.js"></script> -->
    <script src="{{ asset('assets/css/plugins/jquery-slimscroll/jquery.slimscroll.js') }}" type="text/javascript"></script>

    <!-- Waves Effect Plugin Js -->
    <!-- <script src="../../plugins/node-waves/waves.js"></script> -->
    <script src="{{ asset('assets/css/plugins/node-waves/waves.js') }}" type="text/javascript"></script>

    <!-- Autosize Plugin Js -->
    <!-- <script src="../../plugins/autosize/autosize.js"></script> -->
    <script src="{{ asset('assets/css/plugins/autosize/autosize.js') }}" type="text/javascript"></script>


    <!-- Moment Plugin Js -->
    <!-- <script src="../../plugins/momentjs/moment.js"></script> -->
    <script src="{{ asset('assets/css/plugins/momentjs/moment.js') }}" type="text/javascript"></script>




    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="{{ asset('assets/css/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}" type="text/javascript"></script>
    
    <!-- Custom Js -->
    <!-- <script src="{{ asset('assets/js/admin.js') }}" type="text/javascript"></script> -->
    <script src="{{ asset('assets/js/pages/forms/basic-form-elements.js')}}" type="text/javascript"></script>


    
@endsection