@extends('layouts.sidebar')

@section('style')
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" />
@endsection

@section('section')
<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>
                    ORDERS
                    
                </h2>
            </div>
            
            <!-- Advanced Validation -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">

                            <h2>ADD NEW DETAIL</h2>
                            
                        </div>
                        <div class="body">
                            <form action="{{ route('orderd.store.orderd', $id_order_h) }}" enctype="multipart/form-data" method="post"> 
                 
                 				{{ csrf_field() }}
                                
                                
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="type" id="type" maxlength="50" minlength="3" required>
                                        <label class="form-label">Type</label>
                                    </div>
                                    <div class="help-info">Min. 3 characters</div>
                                    @if ($errors->has('type'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('type') }}</strong>
                                        </span><br>
                                    @endif
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="itemname" id="itemname" maxlength="50" minlength="3" required>
                                        <label class="form-label">Item name</label>
                                    </div>
                                    <div class="help-info">Min. 3 characters</div>
                                    @if ($errors->has('itemname'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('itemname') }}</strong>
                                        </span><br>
                                    @endif
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="number" class="form-control" name="quantity" id="quantity" maxlength="100" minlength="3" required>
                                        <label class="form-label">Quantity</label>
                                    </div>
                                    <div class="help-info">Numeric</div>
                                    @if ($errors->has('quantity'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('quantity') }}</strong>
                                        </span><br>
                                    @endif
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="number" class="form-control" name="sellprice" id="sellprice" maxlength="50" minlength="3" required>
                                        <label class="form-label">Sell price</label>
                                    </div>
                                    <div class="help-info">Numeric</div>
                                    @if ($errors->has('sellprice'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('sellprice') }}</strong>
                                        </span><br>
                                    @endif
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="number" class="form-control" name="buyprice" id="buyprice" maxlength="50" minlength="3" required>
                                        <label class="form-label">Buy price</label>
                                    </div>
                                    <div class="help-info">Numeric</div>
                                    @if ($errors->has('buyprice'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('buyprice') }}</strong>
                                        </span><br>
                                    @endif
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="commissionto" id="commissionto" maxlength="50" minlength="3" required>
                                        <label class="form-label">Commission to</label>
                                    </div>
                                    <div class="help-info">Min. 3 characters</div>
                                    @if ($errors->has('commissionto'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('commissionto') }}</strong>
                                        </span><br>
                                    @endif
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="number" class="form-control" name="commission" id="commission" maxlength="50" minlength="3" required>
                                        <label class="form-label">Commission amount</label>
                                    </div>
                                    <div class="help-info">Numeric</div>
                                    @if ($errors->has('commission'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('commission') }}</strong>
                                        </span><br>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Note</label>
                                    <div class="form-line">
                                        <textarea rows="4" name="note" class="form-control no-resize" placeholder="Note"></textarea>
                                    </div>
                                </div>
                                

                                
                                <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Advanced Validation -->
            
        </div>
    </section>

@endsection

@section('script')

    <!-- Jquery Core Js -->
    <script src="{{ asset('assets/css/plugins/jquery/jquery.min.js') }}" type="text/javascript"></script>
    <!-- Bootstrap Core Js -->
    <!-- <script src="{{ asset('assets/css/plugins/bootstrap/js/bootstrap.js') }}" type="text/javascript"></script> -->
    <!-- Select Plugin Js -->
    <!-- <script src="../../plugins/bootstrap-select/js/bootstrap-select.js"></script> -->
    <script src="{{ asset('assets/css/plugins/bootstrap-select/js/bootstrap-select.js') }}" type="text/javascript"></script>

    <!-- Slimscroll Plugin Js -->
    <!-- <script src="../../plugins/jquery-slimscroll/jquery.slimscroll.js"></script> -->
    <script src="{{ asset('assets/css/plugins/jquery-slimscroll/jquery.slimscroll.js') }}" type="text/javascript"></script>

    <!-- Waves Effect Plugin Js -->
    <!-- <script src="../../plugins/node-waves/waves.js"></script> -->
    <script src="{{ asset('assets/css/plugins/node-waves/waves.js') }}" type="text/javascript"></script>

    <!-- Autosize Plugin Js -->
    <!-- <script src="../../plugins/autosize/autosize.js"></script> -->
    <script src="{{ asset('assets/css/plugins/autosize/autosize.js') }}" type="text/javascript"></script>


    <!-- Moment Plugin Js -->
    <!-- <script src="../../plugins/momentjs/moment.js"></script> -->
    <script src="{{ asset('assets/css/plugins/momentjs/moment.js') }}" type="text/javascript"></script>




    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="{{ asset('assets/css/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}" type="text/javascript"></script>
    
    <!-- Custom Js -->
    <!-- <script src="{{ asset('assets/js/admin.js') }}" type="text/javascript"></script> -->
    <script src="{{ asset('assets/js/pages/forms/basic-form-elements.js')}}" type="text/javascript"></script>



    
@endsection