@extends('layouts.sidebar')

@section('style')
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" />
@endsection

@section('section')
<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>
                    ORDERS
                    
                </h2>
            </div>
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                DETAILS
                            </h2>
                            <ul class="header-dropdown m-r--4">
                                <a href="{{ route('orderh.edit', $orderh->id) }}">
                                    <button class="btn btn-warning btn-sm" title='Edit header'><i class="material-icons">edit</i></button>
                                </a>
                               <a href="{{ route('orderd.create.orderd',$orderh->id) }}">
                                    <button class="btn btn-primary btn-sm" title="Add new detail"><i class="material-icons" style="color:white;">add_box</i></button>
                                </a>
                                <a href="{{ route('orderh.index') }}">
                                    <button class="btn btn-success btn-sm" title="Back to orders"><i class="material-icons" style="color:white;">backspace</i></button>
                                </a>
                            </ul>
                        </div>
                        <div class="body">
                            <div>
                                <div class="container">
                                    <table class="table table-bordered" style="width: 500px;  margin-left:25%">
                                        <tr>
                                            <td>Order no. </td>
                                            <td>{{ $orderh->id }} / {{$orderh->date->format('m')}} / {{$orderh->date->format('y')}}</td>
                                        </tr>
                                        <tr>
                                            <td>Customer name </td>
                                            <td>{{$orderh->name}}</td>
                                        </tr>
                                        <tr>
                                            <td>Date </td>
                                            <td>{{$orderh->date->format('d-M-Y')}}</td>
                                        </tr>
                                        <tr>
                                            <td>Added by </td>
                                            <td>{{$orderh->addedby}}</td>
                                        </tr>
                                        <tr>
                                            <td>Payment method</td><td> {{$orderh->paymentmethod}}</td>
                                        </tr>
                                        <tr>
                                            <td>Total sell price</td><td> Rp. {{number_format( $ttlsp )}}</td>
                                        </tr>
                                        <tr>
                                            <td>Total buy price</td><td> Rp. {{number_format( $ttlbp )}}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example">
                                    <thead>
                                        <tr>
                                          <th>Type</th>
                                          <th>Item Name</th>
                                          <th>Quantity</th>
                                          <th>Sell Price</th>
                                          <th>Buy price</th>
                                          <th>Commission to</th>
                                          <th>Commission</th>
                                          <th>Note</th>
                                          <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($orderd as $orderd)
                                        <tr>
                                            
                                            <td>{{ $orderd->type }}</td>
                                            <td>{{ $orderd->itemname }}</td>
                                            <td>{{ $orderd->qty }}</td>
                                            <td>Rp. {{number_format( $orderd->sellprice )}}</td>
                                            <td>Rp. {{number_format( $orderd->buyprice )}}</td>
                                            <td>{{ $orderd->commissionto }}</td>
                                            <td>Rp. {{number_format( $orderd->commission )}}</td>
                                            <td>{{ $orderd->note }}</td>
                                             <td width="100px">
                                                
                                                <a href="{{ route('orderd.edit.orderd', [$orderh->id, $orderd->id]) }}">
                                                    <button class="btn btn-warning btn-sm" title="Edit"><i class="material-icons">edit</i></button>
                                                </a>
                                                <form  style="display:inline-block;" action="{{ route('orderd.destroy.orderd',[$orderh->id, $orderd->id]) }}" method="POST" >
                                                  {{ method_field('DELETE') }}
                                                  {{ csrf_field() }}
                                                   <button onclick="return confirm('Are you sure to delete {{ $orderd->type }}, {{ $orderd->itemname }}? ')" class="btn btn-danger btn-sm" title='Delete data'><i class="material-icons">delete_forever</i></button>
                                                </form>
                                                
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
            
        </div>

    </section>


@endsection

@section('script')

    
    <!-- Jquery Core Js -->
    <script src="{{ asset('assets/css/plugins/jquery/jquery.min.js') }}" type="text/javascript"></script>
    <!-- <script src="../../plugins/jquery/jquery.min.js"></script> -->

    <!-- Bootstrap Core Js -->
    <!-- <script src="{{ asset('assets/css/plugins/bootstrap/js/bootstrap.js') }}" type="text/javascript"></script> -->
    <!-- <script src="../../plugins/bootstrap/js/bootstrap.js"></script> -->
   
    <!-- Select Plugin Js -->
    <!-- <script src="../../plugins/bootstrap-select/js/bootstrap-select.js"></script> -->
    <!-- <script src="{{ asset('assets/css/plugins/bootstrap-select/js/bootstrap-select.js') }}" type="text/javascript"></script> -->

    <!-- Slimscroll Plugin Js -->
    <!-- <script src="../../plugins/jquery-slimscroll/jquery.slimscroll.js"></script> -->
    <script src="{{ asset('assets/css/plugins/jquery-slimscroll/jquery.slimscroll.js') }}" type="text/javascript"></script>

    <!-- Waves Effect Plugin Js -->
    <!-- <script src="../../plugins/node-waves/waves.js"></script> -->
    <script src="{{ asset('assets/css/plugins/node-waves/waves.js') }}" type="text/javascript"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('assets/css/plugins/jquery-datatable/jquery.dataTables.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/css/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/css/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/css/plugins/jquery-datatable/extensions/export/jszip.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/css/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/css/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/css/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/css/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}" type="text/javascript"></script>

    <!-- Custom Js -->
    <script src="{{ asset('assets/js/pages/tables/jquery-datatable.js') }}" type="text/javascript"></script>

@endsection