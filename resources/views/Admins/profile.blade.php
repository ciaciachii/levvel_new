@extends('Partner.layouts.sidebar')

@section('style')
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" />
@endsection

@section('section')
<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>
                    ADMIN
                    
                </h2>
            </div>
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                PROFILE
                            </h2>
                            
                        </div>
                        <div class="body">
                            @if (session('alert'))
                        <div class="alert alert-danger">
                            {{ session('alert') }}
                        </div>
                        @endif
                        @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                        @endif
                        <div class="row user-infos cyruxx">
            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 col-xs-offset-0 col-sm-offset-0 col-md-offset-1 col-lg-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">User information</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                        	
            
                            <div class="col-md-3 col-lg-3 hidden-xs hidden-sm">
                                <img class="img-circle" src= "{{ asset($img) }}?dummy={{$rand}}" alt="Admin" style="float:left; max-height:150px; max-width:150px;">
                            </div>
                            <div class="col-xs-2 col-sm-2 hidden-md hidden-lg">
                                <img class="img-circle" src= "{{ asset($img) }}" alt="Admin" style="float:left; max-height:150px; max-width:150px;">
                            </div>
                            <div class="col-xs-10 col-sm-10 hidden-md hidden-lg">
                                <strong>{{$admin->PartnerUserName}}</strong><br>
                                <dl>
                                    <dt>User level:</dt>
                                    <dd>{{e($privilege)}}</dd>
                                    <dt>Registered since:</dt>
                                    <dd>{{e($admin->CreatedDate)}}</dd>
                                    <dt>Email</dt>
                                    <dd>{{e($admin->Email)}}</dd>
                                    
                                </dl>
                            </div>
                            <div class=" col-md-9 col-lg-9 hidden-xs hidden-sm">
                                <strong>{{e($admin->PartnerUserName)}}</strong><br>
                                <table class="table table-user-information">
                                    <tbody>
                                    <tr>
                                        <td>User level:</td>
                                        <td>{{e($privilege)}}</td>
                                    </tr>
                                    <tr>
                                        <td>Registered since:</td>
                                        <td>{{e($admin->CreatedDate)}}</td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td>{{e($admin->Email)}}</td>
                                    </tr>
                                    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <a href="{{ route('partner.editprof', $admin->PartnerUserID) }}">
                            <button class="btn btn-sm btn-primary" type="button"
                                    data-toggle="tooltip"
                                    data-original-title="Send message to user" title="Change profile picture"><i class="glyphicon glyphicon-picture" ></i>
                            </button>
                        </a>
                        <span class="pull-right">
                            <a href="{{ route('partner.Admin.edit', $admin->PartnerUserID) }}">
                                
                                <button class="btn btn-sm btn-warning" type="button"
                                        data-toggle="tooltip"
                                        data-original-title="Edit this user" title="Change profile data"><i class="glyphicon glyphicon-edit"></i>
                                </button>
                            </a>
                            <a href="{{ route('partner.editpass', $admin->PartnerUserID) }}">
                                <button class="btn btn-sm btn-danger" type="button"
                                        data-toggle="tooltip"
                                        data-original-title="Remove this user" title="Change password"><i class="glyphicon glyphicon-lock"></i>
                                </button>
                            </a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
            
        </div>

    </section>


@endsection

@section('script')
<script>
function myFunction() {
    alert("Testing sections!");
}
</script>
	
    <!-- Jquery Core Js -->
    <script src="{{ asset('assets/css/plugins/jquery/jquery.min.js') }}" type="text/javascript"></script>
    <!-- <script src="../../plugins/jquery/jquery.min.js"></script> -->

    <!-- Bootstrap Core Js -->
    <!-- <script src="{{ asset('assets/css/plugins/bootstrap/js/bootstrap.js') }}" type="text/javascript"></script> -->
    <!-- <script src="../../plugins/bootstrap/js/bootstrap.js"></script> -->
   
    <!-- Select Plugin Js -->
    <!-- <script src="../../plugins/bootstrap-select/js/bootstrap-select.js"></script> -->
    <!-- <script src="{{ asset('assets/css/plugins/bootstrap-select/js/bootstrap-select.js') }}" type="text/javascript"></script> -->

    <!-- Slimscroll Plugin Js -->
    <!-- <script src="../../plugins/jquery-slimscroll/jquery.slimscroll.js"></script> -->
    <script src="{{ asset('assets/css/plugins/jquery-slimscroll/jquery.slimscroll.js') }}" type="text/javascript"></script>

    <!-- Waves Effect Plugin Js -->
    <!-- <script src="../../plugins/node-waves/waves.js"></script> -->
    <script src="{{ asset('assets/css/plugins/node-waves/waves.js') }}" type="text/javascript"></script>

	<!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('assets/css/plugins/jquery-datatable/jquery.dataTables.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/css/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/css/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/css/plugins/jquery-datatable/extensions/export/jszip.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/css/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/css/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/css/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/css/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}" type="text/javascript"></script>

    <!-- Custom Js -->
    <script src="{{ asset('assets/js/pages/tables/jquery-datatable.js') }}" type="text/javascript"></script>

@endsection