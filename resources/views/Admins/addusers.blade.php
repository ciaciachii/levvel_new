@extends('Partner.layouts.sidebar')

@section('style')
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" />
@endsection

@section('section')
<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>
                    DATA
                    
                </h2>
            </div>
            
            <!-- Advanced Validation -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>ADD NEW ADMIN</h2>
                        </div>
                        <div class="body">
                            <form action="{{ route('partner.Admin.store') }}" enctype="multipart/form-data" method="post"> 
                 
                 				{{ csrf_field() }}
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="PartnerUserName" id="PartnerUserName" maxlength="191" minlength="3" required>
                                        <label class="form-label">Name</label>
                                    </div>
                                    <div class="help-info">Min. 3 characters</div>
                                    @if ($errors->has('PartnerUserName'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('PartnerUserName') }}</strong>
                                        </span><br>
                                    @endif
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="email" class="form-control" name="PartnerUserEmail" id="PartnerUserEmail" maxlength="191" required>
                                        <label class="form-label">Email</label>
                                    </div>
                                    <div class="help-info">Enter a valid email</div>
                                    @if ($errors->has('PartnerUserEmail'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('PartnerUserEmail') }}</strong>
                                        </span><br>
                                    @endif
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                    	
                                        <input type="password" class="form-control" name="password" id="password" minlength="6" required>
                                        <label class="form-label">Password</label>
                                    </div>
                                    <div class="help-info">Min. 6 characters</div>
                                    @if ($errors->has('password'))
				                        <span class="help-block">
				                            <strong>{{ $errors->first('password') }}</strong>
				                        </span>
				                    @endif
                                </div>
                               	<div class="form-group form-float">
                                    <div class="form-line">
                                    	
                                        <input type="password" class="form-control"  id="password_confirmation" name="password_confirmation"  minlength="3" required>
                                        <label class="form-label">Confirm Password</label>
                                    </div>
                                    <div class="help-info">Min. 6 characters, must be identical with Password</div>
                                    @if ($errors->has('password_confirmation'))
				                        <span class="help-block">
				                            <strong>{{ $errors->first('password_confirmation') }}</strong>
				                        </span>
				                    @endif

                                </div>
                               
                                <div class="form-group">
                                    <input type="radio" name="role" id="sa" class="with-gap" value="sa">
                                    <label for="sa">Super Admin</label>

                                    <input type="radio" name="role" id="a" class="with-gap" value="a" checked>
                                    <label for="a" class="m-l-20">Admin</label>
                                    @if ($errors->has('role'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('role') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                
                                <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Advanced Validation -->
            
        </div>
    </section>

@endsection

@section('script')

	
    <!-- Jquery Core Js -->
    <script src="{{ asset('assets/css/plugins/jquery/jquery.min.js') }}" type="text/javascript"></script>
    <!-- <script src="../../plugins/jquery/jquery.min.js"></script> -->

    <!-- Bootstrap Core Js -->
    <!-- <script src="{{ asset('assets/css/plugins/bootstrap/js/bootstrap.js') }}" type="text/javascript"></script> -->
    <!-- <script src="../../plugins/bootstrap/js/bootstrap.js"></script> -->
   
    <!-- Select Plugin Js -->
    <!-- <script src="../../plugins/bootstrap-select/js/bootstrap-select.js"></script> -->
    <script src="{{ asset('assets/css/plugins/bootstrap-select/js/bootstrap-select.js') }}" type="text/javascript"></script>

    <!-- Slimscroll Plugin Js -->
    <!-- <script src="../../plugins/jquery-slimscroll/jquery.slimscroll.js"></script> -->
    <script src="{{ asset('assets/css/plugins/jquery-slimscroll/jquery.slimscroll.js') }}" type="text/javascript"></script>

    <!-- Dropzone Plugin Js -->
    <script src="{{ asset('assets/css/plugins/dropzone/dropzone.js') }}" type="text/javascript"></script>
    
    <!-- Waves Effect Plugin Js -->
    <!-- <script src="../../plugins/node-waves/waves.js"></script> -->
    <script src="{{ asset('assets/css/plugins/node-waves/waves.js') }}" type="text/javascript"></script>

	<!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('assets/css/plugins/jquery-datatable/jquery.dataTables.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/css/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/css/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/css/plugins/jquery-datatable/extensions/export/jszip.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/css/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/css/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/css/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/css/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}" type="text/javascript"></script>

    <!-- Custom Js -->
    <script src="{{ asset('assets/js/pages/tables/jquery-datatable.js') }}" type="text/javascript"></script>
    
@endsection