@extends('Partner.layouts.sidebar')

@section('style')
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" />
@endsection

@section('section')
<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>
                    DATA
                    
                </h2>
            </div>
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                ADMINS
                            </h2>
                            @if (auth('partner')->user()->can('create.partneradmin'))
                            <ul class="header-dropdown m-r--4">
                               <a href="{{ route('partner.Admin.create') }}">
					                <button class="btn btn-primary btn-sm" title="Add"><i class="material-icons" style="color:white;">person_add</i></button>
					            </a>
                            </ul>
                            @endif
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Privilege</th>
                                            <th>Created By</th>
            <th>Created Date</th>
            <th>Modified By</th>
            <th>Modified Date</th>
                                            <th width="64px">Actions</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Privilege</th>
                                            <th>Created By</th>
            <th>Created Date</th>
            <th>Modified By</th>
            <th>Modified Date</th>
                                            <th>Actions</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        @foreach($admin as $acc)
                                        <tr>
                                            <td>{{ $acc->PartnerUserName }}</td>
								            <td>{{ $acc->PartnerUserEmail }}</td>
								            
								            <td>{{ $acc->role_id == '5'? 'Super Admin' : 'Admin'}}</td>
                                            <td>{{ $acc->CreatedBy }}</td>
                                            <td>{{ $acc->CreatedDate }}</td>
                                            <td>{{ $acc->ModifiedBy }}</td>
                                            <td>{{ $acc->ModifiedDate }}</td>
                                            <td >
			            	
								            	<a href="{{ route('partner.Admin.edit', $acc->PartnerUserID) }}">
					                    			<button class="btn btn-warning btn-sm" title='Edit data'><i class="material-icons">edit</i></button>
					                			</a>
												
					                        	<form  style="display:inline-block;" action="{{ route('partner.Admin.destroy', $acc->PartnerUserID) }}" method="POST" >
							                      {{ method_field('DELETE') }}
							                      {{ csrf_field() }}
							                       <button onclick="return confirm('Are you sure to delete {{$acc->PartnerUserName}}?')" class="btn btn-danger btn-sm" title='Delete data'><i class="material-icons">delete_forever</i></button>
							                  	</form>
								            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
            
        </div>

    </section>


@endsection

@section('script')
<script>
function myFunction() {
    alert("Testing sections!");
}
</script>
	
    <!-- Jquery Core Js -->
    <script src="{{ asset('assets/css/plugins/jquery/jquery.min.js') }}" type="text/javascript"></script>
    <!-- <script src="../../plugins/jquery/jquery.min.js"></script> -->

    <!-- Bootstrap Core Js -->
    <!-- <script src="{{ asset('assets/css/plugins/bootstrap/js/bootstrap.js') }}" type="text/javascript"></script> -->
    <!-- <script src="../../plugins/bootstrap/js/bootstrap.js"></script> -->
   
    <!-- Select Plugin Js -->
    <!-- <script src="../../plugins/bootstrap-select/js/bootstrap-select.js"></script> -->
    <!-- <script src="{{ asset('assets/css/plugins/bootstrap-select/js/bootstrap-select.js') }}" type="text/javascript"></script> -->

    <!-- Slimscroll Plugin Js -->
    <!-- <script src="../../plugins/jquery-slimscroll/jquery.slimscroll.js"></script> -->
    <script src="{{ asset('assets/css/plugins/jquery-slimscroll/jquery.slimscroll.js') }}" type="text/javascript"></script>

    <!-- Waves Effect Plugin Js -->
    <!-- <script src="../../plugins/node-waves/waves.js"></script> -->
    <script src="{{ asset('assets/css/plugins/node-waves/waves.js') }}" type="text/javascript"></script>

	<!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('assets/css/plugins/jquery-datatable/jquery.dataTables.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/css/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/css/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/css/plugins/jquery-datatable/extensions/export/jszip.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/css/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/css/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/css/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/css/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}" type="text/javascript"></script>

    <!-- Custom Js -->
    <script src="{{ asset('assets/js/pages/tables/jquery-datatable.js') }}" type="text/javascript"></script>

@endsection