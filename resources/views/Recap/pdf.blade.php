<html>
	<head>
	
		
		
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

	</head>
    <style>
        table { 
          width: 75%; 
          border-collapse: collapse; 
          margin: auto;
          text-align:center;
          vertical-align:center;
          border-collapse: collapse; 
        }

        /* zebra striping */
        tr:nth-of-type(odd) { 
          background: #f0f0f0; 
        }

        th { 
          background: #666; 
          color: white; 
          font-weight: bold; 
        }

        td, th { 
          padding: 6px; 
          border: 1px solid #ccc; 
          text-align: left; 
        }
    </style>
	<body>
        <h1 style="text-align:center;">Levvel Recap {{$start}} to {{$finish}}</h1>
        <h2 style="text-align:center;">Orders</h2>
		<table>
      <thead>
        <tr>
          <th>Order no.</th>
          <th>Customer Name</th>
          <th>Date</th>
          <th>Profits</th>
        </tr>
      </thead>
      <tbody>
        @foreach($orderh as $oh)
          <tr>
            <td>{{ $oh->id }} / {{$oh->date->format('m')}} / {{$oh->date->format('y')}}</td>
            <td>{{ $oh->name }}</td>
            <td>{{ $oh->date->format('d-M-Y') }}</td>
            <td>Rp. {{ number_format($oh->profit) }}</td>
          </tr>
        @endforeach

      </tbody>

    </table>
    <br><br>
    <h2 style="text-align:center;">Incomplete Orders</h2>
    <h3 style="text-align:center;">Manually add to next recap</h3>
    <table>
      <thead>
        <tr>
          <th>Order no.</th>
          <th>Customer Name</th>
          <th>Date</th>
          <th>Profits</th>
        </tr>
      </thead>
      <tbody>
        @foreach($orderhinc as $oh)
          <tr>
            <td>{{ $oh->id }} / {{$oh->date->format('m')}} / {{$oh->date->format('y')}}</td>
            <td>{{ $oh->name }}</td>
            <td>{{ $oh->date->format('d-M-Y') }}</td>
            <td>Rp. {{ number_format($oh->profit) }}</td>
          </tr>
        @endforeach

      </tbody>

    </table>

    <br><br>

          <h2 style="text-align:center;">Expenditures</h2>

              <table>
                <thead>
                    <tr>
                      <th>Expenditure ID</th>
                      <th>Date</th>
                      <th>Name</th>
                      <th>Amount</th>
                      <th>Added by</th>
                      <th>Note</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach($expenditure as $ex)
                        <tr>
                          <td>{{ $ex->id }}</td>
                          <td>{{ $ex->date }}</td> 
                          <td>{{ $ex->name }}</td>
                          <td>Rp. {{ number_format($ex->spend) }}</td>  
                          <td>{{ $ex->addedby }}</td> 
                          <td>{{ $ex->note }}</td>  
                        </tr>
                    @endforeach

                </tbody>

              </table>
              <br><br>
              <h2 style="text-align:center;">Commission</h2>

              <table>
                <thead>
                    <tr>
                      <th>Commission to</th>
                      <th>Amount</th>
                      
                    </tr>
                </thead>
                <tbody>

                    @foreach($commission as $ex)
                        <tr>
                          <td>{{ $ex->commissionto }}</td>
                          
                          <td>Rp. {{ number_format($ex->amount) }}</td>   
                        </tr>
                    @endforeach

                </tbody>

              </table>

          <p  style="text-align:center;">
          Total income: Rp. {{number_format($totals[0]->total)}}<br>
          Total expenditure: Rp. {{number_format($expe[0]->amt)}}<br>
          Total profit: Rp. {{number_format($ttlexpe[0]->total)}}<br>
          Share Toko: Rp. {{ number_format(($ttlexpe[0]->total)*0.2) }}<br>
          Share Cia: Rp. {{ number_format(($ttlexpe[0]->total)*0.8*($cia/100)) }}<br>
          Share Theo: Rp. {{ number_format(($ttlexpe[0]->total)*0.8*($theo/100)) }}<br>
          Share Lodi: Rp. {{ number_format(($ttlexpe[0]->total)*0.8*($lodi/100)) }}
          </p>
	</body>
</html>