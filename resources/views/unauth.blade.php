<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>404 | V-Tal</title>
    <!-- Favicon-->
    <link rel="shortcut icon" href="{{ asset('assets/image/favicon/apple-icon-57x57.png') }}">
    <!-- Favicon -->   
    <link rel="apple-touch-icon" type="image/png" sizes="57x57" href="{{ asset('/image/favicon/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" type="image/png" sizes="60x60" href="{{ asset('/image/favicon/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" type="image/png" sizes="72x72" href="{{ asset('/image/favicon/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" type="image/png" sizes="76x76" href="{{ asset('/image/favicon/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" type="image/png" sizes="114x114" href="{{ asset('/image/favicon/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" type="image/png" sizes="120x120" href="{{ asset('/image/favicon/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" type="image/png" sizes="144x144" href="{{ asset('/image/favicon/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" type="image/png" sizes="152x152" href="{{ asset('/image/favicon/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" type="image/png" sizes="180x180" href="{{ asset('/image/favicon/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" type="image/png" sizes="192x192"  href="{{ asset('/image/favicon/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" type="image/png" sizes="32x32" href="{{ asset('/image/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" type="image/png" sizes="96x96" href="{{ asset('/image/favicon/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" type="image/png" sizes="16x16" href="{{ asset('/image/favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" type="image/png" content="{{ asset('/image/favicon/ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">
    <!-- /Favicon -->

   <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- /  Google fonts -->
    <!-- Bootstrap Core Css -->
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/bootstrap/css/bootstrap.css') }}" />

    <!-- Waves Effect Css -->
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/node-waves/waves.css') }}" />

     <!-- Custom Css -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}" />
</head>

<body class="four-zero-four">
    <div class="four-zero-four-container">
        <div class="error-code">401</div>
        <div class="error-message">Unauthorized access</div>
        <!-- <div class="button-place">
            <a href="../../index.html" class="btn btn-default btn-lg waves-effect">GO TO HOMEPAGE</a>
        </div> -->
    </div>

    <!-- Jquery Core Js -->
    <script src="{{ asset('assets/css/plugins/jquery/jquery.min.js') }}" type="text/javascript"></script>
    
    <!-- Bootstrap Core Js -->
    <script src="{{ asset('assets/css/plugins/bootstrap/js/bootstrap.js') }}" type="text/javascript"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ asset('assets/css/plugins/node-waves/waves.js') }}" type="text/javascript"></script>
</body>

</html>