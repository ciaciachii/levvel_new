<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Sign In | Levvel</title>
   <!-- Favicon -->   
    <link rel="apple-touch-icon" type="image/png" sizes="57x57" href="{{ asset('assets/image/favicon/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" type="image/png" sizes="60x60" href="{{ asset('assets/image/favicon/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" type="image/png" sizes="72x72" href="{{ asset('assets/image/favicon/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" type="image/png" sizes="76x76" href="{{ asset('assets/image/favicon/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" type="image/png" sizes="114x114" href="{{ asset('assets/image/favicon/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" type="image/png" sizes="120x120" href="{{ asset('assets/image/favicon/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" type="image/png" sizes="144x144" href="{{ asset('assets/image/favicon/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" type="image/png" sizes="152x152" href="{{ asset('assets/image/favicon/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" type="image/png" sizes="180x180" href="{{ asset('assets/image/favicon/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" type="image/png" sizes="192x192"  href="{{ asset('assets/image/favicon/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" type="image/png" sizes="32x32" href="{{ asset('assets/image/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" type="image/png" sizes="96x96" href="{{ asset('assets/image/favicon/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" type="image/png" sizes="16x16" href="{{ asset('assets/image/favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" type="image/png" content="{{ asset('assets/image/favicon/vtal/ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">
    <!-- /Favicon -->
    <!-- Google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <!-- /  Google fonts -->
    <!-- Bootstrap Core Css -->
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/bootstrap/css/bootstrap.css') }}" />

    <!-- Waves Effect Css -->
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/node-waves/waves.css') }}" />

    <!-- Animation Css -->
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/animate-css/animate.css') }}" />
    <!-- Morris Chart Css -->
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/morrisjs/morris.css') }}" />
    <!-- Custom Css -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}" />
    <!-- AdminBSB Themes -->
    <link rel="stylesheet" href="{{ asset('assets/css/themes/all-themes.css') }}" />
</head>

<body class="login-page">
    <div class="login-box">
        <div class="logo" style="text-align:center;">
            <img class="dl-gp" src= "{{ asset('assets/image/levvel.png') }}" alt="App Features"  style="display: inline-block;z-index:99;" width="75px">
            
        </div>
        <div class="card">
            <div class="body">
                <form id="sign_in" method="POST" action="login">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="msg">Sign in to start your admin session</div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">face</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="name" placeholder="Username" required autofocus>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="pass" placeholder="Password" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-8 p-t-5">
                            <input type="checkbox" name="rememberme" id="rememberme" class="filled-in chk-col-red">
                            <label for="rememberme">Remember Me</label>
                        </div>
                        <div class="col-xs-4">
                            <button class="btn btn-block bg-red waves-effect" type="submit">SIGN IN</button>
                        </div>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
   
    <!-- Jquery Core Js -->
    <!-- <script src="../../plugins/jquery/jquery.min.js"></script> -->
    <script src="{{ asset('assets/css/plugins/jquery/jquery.min.js') }}" type="text/javascript"></script>

    <!-- Bootstrap Core Js -->
    <!-- <script src="../../plugins/bootstrap/js/bootstrap.js"></script> -->
    <script src="{{ asset('assets/css/plugins/bootstrap/js/bootstrap.js') }}" type="text/javascript"></script>

    <!-- Waves Effect Plugin Js -->
    <!-- <script src="../../plugins/node-waves/waves.js"></script> -->
    <script src="{{ asset('assets/css/plugins/node-waves/waves.js') }}" type="text/javascript"></script>

    <!-- Validation Plugin Js -->
    <!-- <script src="../../plugins/jquery-validation/jquery.validate.js"></script> -->
    <script src="{{ asset('assets/css/plugins/jquery-validation/jquery.validate.js') }}" type="text/javascript"></script>

    <!-- SweetAlert Plugin Js -->
    <script src="{{ asset('assets/css/plugins/sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
    @include('sweet::alert')
    <!-- Custom Js -->
    <!-- <script src="../../js/admin.js"></script> -->
    <script src="{{ asset('assets/js/admin.js') }}" type="text/javascript"></script>
    <!-- <script src="../../js/pages/examples/sign-in.js"></script> -->
    <script src="{{ asset('assets/js/pages/examples/sign-in.js') }}" type="text/javascript"></script>

</body>

</html>