<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>LevvelApp</title>
    <!-- Favicon-->
    <link rel="shortcut icon" href="{{ asset('assets/image/favicon/apple-icon-57x57.png') }}">
    <!-- Favicon -->   
    <link rel="apple-touch-icon" type="image/png" sizes="57x57" href="{{ asset('/image/favicon/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" type="image/png" sizes="60x60" href="{{ asset('/image/favicon/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" type="image/png" sizes="72x72" href="{{ asset('/image/favicon/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" type="image/png" sizes="76x76" href="{{ asset('/image/favicon/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" type="image/png" sizes="114x114" href="{{ asset('/image/favicon/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" type="image/png" sizes="120x120" href="{{ asset('/image/favicon/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" type="image/png" sizes="144x144" href="{{ asset('/image/favicon/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" type="image/png" sizes="152x152" href="{{ asset('/image/favicon/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" type="image/png" sizes="180x180" href="{{ asset('/image/favicon/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" type="image/png" sizes="192x192"  href="{{ asset('/image/favicon/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" type="image/png" sizes="32x32" href="{{ asset('/image/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" type="image/png" sizes="96x96" href="{{ asset('/image/favicon/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" type="image/png" sizes="16x16" href="{{ asset('/image/favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" type="image/png" content="{{ asset('/image/favicon/ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">
    <!-- /Favicon -->
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- /  Google fonts -->
    <!-- Bootstrap Core Css -->
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/bootstrap/css/bootstrap.css') }}" />

    <!-- Waves Effect Css -->
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/node-waves/waves.css') }}" />
    
    <!-- Animation Css -->
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/animate-css/animate.css') }}" />
    <!-- Morris Chart Css-->
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/morrisjs/morris.css') }}" />
    <!-- Custom Css -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}" />
    <!-- AdminBSB Themes-->
    <link rel="stylesheet" href="{{ asset('assets/css/themes/all-themes.css') }}" />
    <!-- Font Awesome Icons-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    @yield('style')
    
</head>

<body class="theme-red">
    <!-- Page Loader -->
    
    @yield('body')
    
    <!-- Jquery Core Js -->
    <script src="{{ asset('assets/css/plugins/jquery/jquery.min.js') }}" type="text/javascript"></script>
     <!-- Bootstrap Core Js -->
    <script src="{{ asset('assets/css/plugins/bootstrap/js/bootstrap.js') }}" type="text/javascript"></script>
     <!-- Select Plugin Js -->
    <script src="{{ asset('assets/css/plugins/bootstrap-select/js/bootstrap-select.js') }}" type="text/javascript"></script>
    <!-- Slimscroll Plugin Js -->
    <script src="{{ asset('assets/css/plugins/jquery-slimscroll/jquery.slimscroll.js') }}" type="text/javascript"></script>
    <!-- Waves Effect Plugin Js -->
    <script src="{{ asset('assets/css/plugins/node-waves/waves.js') }}" type="text/javascript"></script>
   
   
    <!-- Jquery CountTo Plugin Js -->
    <script src="{{ asset('assets/css/plugins/jquery-countto/jquery.countTo.js') }}" type="text/javascript"></script>
    <!-- Morris Plugin Js -->
    <script src="{{ asset('assets/css/plugins/raphael/raphael.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/css/plugins/morrisjs/morris.js') }}" type="text/javascript"></script>

    <!-- ChartJs -->
    <script src="{{ asset('assets/css/plugins/chartjs/Chart.bundle.js') }}" type="text/javascript"></script>

    <!-- Flot Charts Plugin Js -->
    <script src="{{ asset('assets/css/plugins/flot-charts/jquery.flot.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/css/plugins/flot-charts/jquery.flot.resize.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/css/plugins/flot-charts/jquery.flot.pie.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/css/plugins/flot-charts/jquery.flot.categories.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/css/plugins/flot-charts/jquery.flot.time.js') }}" type="text/javascript"></script>
    @yield('script')
    <!-- Sparkline Chart Plugin Js -->
    <script src="{{ asset('assets/css/plugins/jquery-sparkline/jquery.sparkline.js') }}" type="text/javascript"></script>

    <!-- Custom Js -->
    <script src="{{ asset('assets/js/admin.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/index.js') }}" type="text/javascript"></script>

    <!-- Demo Js -->
    <!-- <script src="js/demo.js"></script> -->
    <script src="{{ asset('assets/plugins/jquery-countto/jquery.countTo.js') }}" type="text/javascript"></script>
    <script src="http://maps.google.com/maps/api/js"></script>

    <!-- Google Maps Key-->
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCKS6IWjM2MdnawgG1PrJjq93wQTlWb1Ys&callback=initMap">
    </script>
    
</body>

</html>