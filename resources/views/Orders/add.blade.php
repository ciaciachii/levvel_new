@extends('layouts.sidebar')

@section('style')
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" />
@endsection

@section('section')
<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>
                    ORDERS
                    
                </h2>
            </div>
            
            <!-- Advanced Validation -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>ADD NEW TRANSACTION</h2>
                            
                        </div>
                        <div class="body">
                            <form action="{{ route('orderh.store') }}" enctype="multipart/form-data" method="post"> 
                 
                 				{{ csrf_field() }}
                                <div class="form-group">
                                    <label class="form-label">Choose existing customer</label>
                                    <!-- <label class="form-label">Ambulance Type</label> -->
                                    <div class="row clearfix">
                                        <div class="col-sm-6">
                                            <select class="form-control show-tick" name="Customer" data-live-search="true"  >

                                                <option selected disabled>Choose existing customer</option>
                                                @foreach ($customer as $cust)
                                                    <option value="{{ $cust->id }}">{{ $cust->name }}  --  {{ $cust->contact }}</option>
                                                @endforeach 
                                            </select>
                                            @if ($errors->has('Customer'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('Customer') }}</strong>
                                            </span><br>
                                            @endif
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group form-float">

                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="name2" id="name2" maxlength="50" minlength="3" >
                                                    <label class="form-label">Add new customer</label>
                                                </div>
                                                <div class="help-info">Min. 3 characters</div>
                                                @if ($errors->has('name2'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('name2') }}</strong>
                                                    </span><br>
                                                @endif
                                            </div>

                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="contact" id="contact" maxlength="50" minlength="3" >
                                                    <label class="form-label">Contact</label>
                                                </div>
                                                <div class="help-info">Min. 3 characters</div>
                                                @if ($errors->has('contact'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('contact') }}</strong>
                                                    </span><br>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                
                                <div class="form-group">
                                    <label class="form-label">Date</label>
                                    <div class="form-line">
                                        
                                        <input type="text" class="datepicker form-control" placeholder="Please choose date..." name="date">                                    </div>
                                    @if ($errors->has('date'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('date') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label class="form-label">Added by</label><!-- <label class="form-label">Ambulance Type</label> -->
                                    <div class="row clearfix">
                                        <select class="form-control show-tick" name="addedby">

                                            <option selected disabled>Added by</option>
                                            <option value="Theo">Theo</option>
                                            <option value="Cia">Cia</option>
                                            <option value="Lodi">Lodi</option>
                                                
                                        </select>
                                        @if ($errors->has('Customer'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('Customer') }}</strong>
                                        </span><br>
                                        @endif
                                    </div>
                                    
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Payment method</label><!-- <label class="form-label">Ambulance Type</label> -->
                                    <div class="row clearfix">
                                        <select class="form-control show-tick" name="paymentmethod">

                                            <option selected disabled>Payment method</option>
                                            <option value="BCA"> BCA</option>
                                            <option value="Cash"> Cash</option>
                                            <option value="Mandiri"> Mandiri</option>
                                            <option value="BliBli"> BliBli</option>
                                            <option value="Bukalapak"> Bukalapak</option>
                                            <option value="Tokopedia"> Tokopedia</option>
                                            <option value="Tempo"> Tempo</option>
                                            <option value="Others"> Others</option>
                                                
                                        </select>
                                        @if ($errors->has('Customer'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('Customer') }}</strong>
                                        </span><br>
                                        @endif
                                    </div>
                                    
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Note</label>
                                    <div class="form-line">
                                        <textarea rows="4" name="note" class="form-control no-resize" placeholder="Note"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Status</label><!-- <label class="form-label">Ambulance Type</label> -->
                                    <div class="row clearfix">
                                        <select class="form-control show-tick" name="status">

                                            <option selected disabled>Status</option>
                                            <option value="0">Completed</option>
                                            <option value="1">Incomplete</option>                                                
                                        </select>
                                        @if ($errors->has('status'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('status') }}</strong>
                                        </span><br>
                                        @endif
                                    </div>
                                    
                                </div>

                                
                                <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Advanced Validation -->
            
        </div>
    </section>

@endsection

@section('script')

    <!-- Jquery Core Js -->
    <script src="{{ asset('assets/css/plugins/jquery/jquery.min.js') }}" type="text/javascript"></script>
    <!-- Bootstrap Core Js -->
    <!-- <script src="{{ asset('assets/css/plugins/bootstrap/js/bootstrap.js') }}" type="text/javascript"></script> -->
    <!-- Select Plugin Js -->
    <!-- <script src="../../plugins/bootstrap-select/js/bootstrap-select.js"></script> -->
    <script src="{{ asset('assets/css/plugins/bootstrap-select/js/bootstrap-select.js') }}" type="text/javascript"></script>

    <!-- Slimscroll Plugin Js -->
    <!-- <script src="../../plugins/jquery-slimscroll/jquery.slimscroll.js"></script> -->
    <script src="{{ asset('assets/css/plugins/jquery-slimscroll/jquery.slimscroll.js') }}" type="text/javascript"></script>

    <!-- Waves Effect Plugin Js -->
    <!-- <script src="../../plugins/node-waves/waves.js"></script> -->
    <script src="{{ asset('assets/css/plugins/node-waves/waves.js') }}" type="text/javascript"></script>

    <!-- Autosize Plugin Js -->
    <!-- <script src="../../plugins/autosize/autosize.js"></script> -->
    <script src="{{ asset('assets/css/plugins/autosize/autosize.js') }}" type="text/javascript"></script>


    <!-- Moment Plugin Js -->
    <!-- <script src="../../plugins/momentjs/moment.js"></script> -->
    <script src="{{ asset('assets/css/plugins/momentjs/moment.js') }}" type="text/javascript"></script>




    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="{{ asset('assets/css/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}" type="text/javascript"></script>
    
    <!-- Custom Js -->
    <!-- <script src="{{ asset('assets/js/admin.js') }}" type="text/javascript"></script> -->
    <script src="{{ asset('assets/js/pages/forms/basic-form-elements.js')}}" type="text/javascript"></script>



    
@endsection