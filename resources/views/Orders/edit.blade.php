@extends('layouts.sidebar')

@section('style')
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" />
@endsection

@section('section')
<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>
                    ORDERS
                    
                </h2>
            </div>
            
            <!-- Advanced Validation -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>EDIT TRANSACTION</h2>
                            
                        </div>
                        <div class="body">
                            <form action="{{ route('orderh.update',$id) }}" enctype="multipart/form-data" method="post"> 
                                <input name="_method" type="hidden" value="PATCH">
                                {{ csrf_field() }}
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="name" id="name" maxlength="50" minlength="3" value="{{$custname ? $custname->name : $name}}" disabled>
                                        <label class="form-label">Customer name</label>
                                    </div>
                                    <div class="help-info">Min. 3 characters</div>
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span><br>
                                    @endif
                                </div>
                                
                                <div class="form-group">
                                    <label class="form-label">Date</label>
                                    <div class="form-line">
                                        
                                        <input type="text" class="datepicker form-control" placeholder="Please choose date..." name="date" id=
                                    "format" value="{{$date}}">
                                    </div>
                                    @if ($errors->has('date'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('date') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label class="form-label">Added by</label>
                                    <div class="row clearfix">
                                        
                                        <select class="form-control show-tick" name="addedby" data-live-search="true">

                                            <option value="Theo" {{$addedby=='Theo' ? 'selected' : ''}}> Theo </option>
                                            <option value="Cia" {{$addedby=='Cia' ? 'selected' : ''}}> Cia</option>
                                            <option value="Lodi" {{$addedby=='Lodi' ? 'selected' : ''}}> Lodi</option>
                                                
                                        </select>

                                        @if ($errors->has('Customer'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('Customer') }}</strong>
                                        </span><br>
                                        @endif
                                    </div>
                                    
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Payment method</label>
                                    <div class="row clearfix">
                                        <select class="form-control show-tick" name="paymentmethod" data-live-search="true">
                                            <option value="BCA" {{$paymentmethod=='BCA' ? 'selected' : ''}}> BCA </option>
                                            <option value="Cash" {{$paymentmethod=='Cash' ? 'selected' : ''}}> Cash</option>
                                            <option value="Mandiri" {{$paymentmethod=='Mandiri' ? 'selected' : ''}}> Mandiri </option>
                                            <option value="BliBli" {{$paymentmethod=='BliBli' ? 'selected' : ''}}> BliBli</option>
                                            <option value="Bukalapak" {{$paymentmethod=='Bukalapak' ? 'selected' : ''}}> Bukalapak </option>
                                            <option value="Tokopedia" {{$paymentmethod=='Tokopedia' ? 'selected' : ''}}> Tokopedia</option>
                                            <option value="Tempo" {{$paymentmethod=='Tempo' ? 'selected' : ''}}> Tempo</option>
                                            <option value="Others" {{$paymentmethod=='Others' ? 'selected' : ''}}> Others</option>
                                                
                                        </select>
                                        @if ($errors->has('Customer'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('Customer') }}</strong>
                                        </span><br>
                                        @endif
                                    </div>
                                    
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Note</label>
                                    <div class="form-line">
                                        <textarea rows="4" name="note" class="form-control no-resize" placeholder="Note"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Status</label>
                                    <!-- <label class="form-label">Ambulance Type</label> -->
                                    <div class="row clearfix">
                                        <select class="form-control show-tick" name="status">

                                            @if ($status == '0')
                                            <option value="0" selected>Completed</option>
                                            <option value="1">Incomplete</option>  
                                            @elseif ($status == '1')
                                            <option value="0" >Completed</option>
                                            <option value="1" selected>Incomplete</option>
                                            @else
                                            <option value="0" >Completed</option>
                                            <option value="1">Incomplete</option>
                                            @endif                                              
                                        </select>
                                        @if ($errors->has('status'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('status') }}</strong>
                                        </span><br>
                                        @endif
                                    </div>
                                    
                                </div>

                                
                                <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Advanced Validation -->
            
        </div>
    </section>

@endsection

@section('script')
    <script>
       
    $(document).ready(function()
        {
            
            
            $('#format').bootstrapMaterialDatePicker({ format : 'DD-MM-YYYY', time:false});
        });
    </script>

    <!-- Jquery Core Js -->
    <script src="{{ asset('assets/css/plugins/jquery/jquery.min.js') }}" type="text/javascript"></script>
    <!-- Bootstrap Core Js -->
    <!-- <script src="{{ asset('assets/css/plugins/bootstrap/js/bootstrap.js') }}" type="text/javascript"></script> -->
    <!-- Select Plugin Js -->
    <!-- <script src="../../plugins/bootstrap-select/js/bootstrap-select.js"></script> -->
    <script src="{{ asset('assets/css/plugins/bootstrap-select/js/bootstrap-select.js') }}" type="text/javascript"></script>

    <!-- Slimscroll Plugin Js -->
    <!-- <script src="../../plugins/jquery-slimscroll/jquery.slimscroll.js"></script> -->
    <script src="{{ asset('assets/css/plugins/jquery-slimscroll/jquery.slimscroll.js') }}" type="text/javascript"></script>

    <!-- Waves Effect Plugin Js -->
    <!-- <script src="../../plugins/node-waves/waves.js"></script> -->
    <script src="{{ asset('assets/css/plugins/node-waves/waves.js') }}" type="text/javascript"></script>

    <!-- Autosize Plugin Js -->
    <!-- <script src="../../plugins/autosize/autosize.js"></script> -->
    <script src="{{ asset('assets/css/plugins/autosize/autosize.js') }}" type="text/javascript"></script>


    <!-- Moment Plugin Js -->
    <!-- <script src="../../plugins/momentjs/moment.js"></script> -->
    <script src="{{ asset('assets/css/plugins/momentjs/moment.js') }}" type="text/javascript"></script>




    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="{{ asset('assets/css/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}" type="text/javascript"></script>
    
    <!-- Custom Js -->
    <!-- <script src="{{ asset('assets/js/admin.js') }}" type="text/javascript"></script> -->
    <script src="{{ asset('assets/js/pages/forms/basic-form-elements.js')}}" type="text/javascript"></script>



    
@endsection