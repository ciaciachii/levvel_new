<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expenditure extends Model
{
    //
    public $table = "expenditure";
    public $fillable = ['id','date','name','amount','addedby','note'];
    public $timestamps = false;
    protected $dates = ['date'];
}
