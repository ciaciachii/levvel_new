<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class OrderD extends Model
{
    //
    public $table = "orderd";
    public $fillable = ['ID','orderh_id','type','itemname','qty','sellprice','buyprice','commissionto','commission','note','Active','delBy','delAt'];
    public $timestamps = false;
    
}
