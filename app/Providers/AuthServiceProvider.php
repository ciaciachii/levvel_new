<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;


use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use App\Permission;
use Schema;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(GateContract $gate)
    {
        // $this->registerPolicies($gate);
     
        if(Schema::hasTable('permissions')){
            $permissions = Permission::all();
            foreach($permissions as $permission) {
                $gate->define($permission->name, function($user) use ($permission) {
                    return $user->hasPermission($permission->name);
                });
            }
        }

     
    }
}
