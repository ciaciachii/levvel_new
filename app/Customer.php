<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Customer extends Model
{
	protected $table = "customer";
    protected $fillable = ['id','name','contact'];
    public $timestamps = false;
    // protected $dates = ['date'];

}

?>