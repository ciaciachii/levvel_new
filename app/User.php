<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    public $fillable = ['id','name','pass','role_id'];
    
        // public $timestamps = false;
        // protected $primaryKey ="UserID";
        // ...
        // protected $fillable = [
        //     'name', 'email', 'password',
        // ];
        public $timestamps = false;
        /**
         * The attributes that should be hidden for arrays.
         *
         * @var array
         */
        protected $hidden = [
            'password', 'remember_token',
        ];
        public function role()
        {
            return $this->belongsTo(Role::class);
        }
     
        public function hasPermission($name)
        {
            return $this->role->permissions->contains('name',$name);
        }


     
    }
    // Auth::user()->hospital()
    // $user->role()
