<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    //
    public $table = "inventory";
    public $fillable = ['ID','date','name','amount','addedby','note'];
    public $timestamps = false;
    protected $dates = ['date'];
}
