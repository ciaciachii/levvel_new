<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class OrderH extends Model
{
	protected $table = "orderh";
    protected $fillable = ['id','name','date','addedby','paymentmethod','Active','delBy','delAt'];
    public $timestamps = false;
    protected $dates = ['date'];

}

?>