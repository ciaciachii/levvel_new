<?php

namespace App\Http\Controllers;
use DB;
use App\User;
use App\OrderH;
use App\Customer;
use App\OrderD;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use App\Http\Requests;
use Auth;


//require 'vendor/autoload.php';

use Carbon\Carbon;
// use App\
use App\Todolist;
use App\Http\Requests\ListFormRequest;
use DateTime;

class CustomerCRUDController extends Controller
{
    public function index()
    {
        if (auth()->user() ==null){
            return redirect()->route('login');
        } 
        $customer =Customer::get();
        
            
        return view('Customers.menu', compact('customer'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (auth()->user() ==null){
            return redirect()->route('login');
        } 
        return view('Customers.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (auth()->user() ==null){
            return redirect()->route('login');
        } 
        // $myDateTime = DateTime::createFromFormat('D d M Y', $request->date);
        $active = 1;
        $customer = Customer::create([
            'id' => $request->id,
            
            'name' => $request->name,
            
            'contact' => $request->contact
            
        ]);
        
        return redirect()->route('customer.index');
    }

    public function edit($id)
    {
        //
        if (auth()->user() ==null){
            return redirect()->route('login');
        } 
        $customer = Customer::findOrFail($id);
       
       
        $name = $customer->name;
        $contact = $customer->contact;
        
        
        
        return view('Customers.edit', compact('id','name','contact'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // echo('edit');
        // $input = $request->all();
        // $orderh = orderh::findOrFail($id);
        // $orderh->fill($input)->save();
        // return redirect()->route('orderh.index')->with('success','Item edited successfully');
        if (auth()->user() ==null){
            return redirect()->route('login');
        } 
        $input = $request->all();
         //dd($input);
         $ex = Customer::findOrFail($id);
        // $orderh->fill($input)->save();
        // return redirect()->route('orderh.index')->with('success','Item edited successfully');

        $ex->name = $request->input('name');
        $ex->contact = $request->input('contact');
        
        //$save=orderh::find($id)->update($request->all());
        $ex->save();


        //Customer::find($id)->update($request->all());
        return redirect()->route('customer.index')
                        ;

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function destroy($customer)
    // {
    //     if (auth()->user() ==null){
    //         return redirect()->route('login');
    //     } 
    //     //  echo('destroy');

    //     // $orderh = orderh::findOrFail($id);
    //     // orderh::destroy($orderh);
    //     // $orderh->delete();
    //     // dd(DB::getQueryLog());
    //     //check if any foreign key
    //     //Session::flash('flash_message', 'Deck successfully deleted!');
    //     $active = 0;
    //     $id = Customer::findOrFail($customer);
    //     // dd($id);
    //     $modBy = auth()->user()->name;
    //     // dd($modBy);
    //     $modAt = Carbon::now(new \DateTimeZone('Asia/Jakarta'));
    //     $id->Active = $active;
    //     $id->delBy = $modBy;
    //     $id->delAt = $modAt;
    //     $id->save();
    //     return redirect()->route('customer.index');
    // }

    
}
