<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use DB;
use App\User;

use App\HospitalUser;
use Auth;
use Hash;
class loginController extends BaseController
{
  //   public function login(Request $req)
  //   {
  //   	$name = $req->input('name');
  //   	$pass = $req->input('pass');
  //   	//dd($req->all());
		// //echo $name." ".$pass;
		// $checkLogin = DB::table('users')->where(['name'=>$name, 'pass'=>$pass])->get();
  //   	if(count($checkLogin) >0)
  //   	{
  //   		return redirect('User/orderh');
  //   	}
  //   	else{
  //           echo "<script>alert('Wrong username / password. Please try again.');</script>";
  //           return view('welcome');
  //       }
    	
  //   }

    public function login(Request $req)
    {
        //$auth = auth()->guard('admin');
        $name = $req->input('name');
        $pass = $req->input('pass');

        // dd("test");
        if($auth = Auth::attempt(['name'=>$name,'password'=>$pass])){
            return redirect('home');
        }
        
        alert()->error('Wrong username and password.', 'Error');
        return view('index');
        
        
    }

    public function logout(){
        // $auth = auth()->guard('admin');
        Auth::guard()->logout();
        return redirect('/');
    }
}

?>