<?php

namespace App\Http\Controllers;
use DB;
use App\User;
use App\OrderH;
use App\OrderD;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class OrderDetailCRUDController extends Controller
{
public function indexOrderD($id_order_h)
    {
        //
        if (auth()->user() ==null){
            return redirect()->route('login');
        } 
        $orderh = orderh::findOrFail($id_order_h);


         //$id = $orderh->id;
        // $name = $orderh->name;
        // $date = $orderh->date;
        // $addedby = $orderh->addedby;
        // $paymentmethod = $orderh->paymentmethod;
        
        // $orderd = DB::table('orderd')->where('orderh_id','=','$id')->get();
        $orderd = orderd::where('orderh_id',$orderh->id)->where('Active',1)->get();
        $sellprice = orderd::select('sellprice')->where('orderh_id',$orderh->id)->where('Active',1)->get();
        $buyprice = orderd::select('buyprice')->where('orderh_id',$orderh->id)->where('Active',1)->get();
        $commission = orderd::select('commission')->where('orderh_id',$orderh->id)->where('Active',1)->get();
        $ttlsp=orderd::select('sellprice')->where('orderh_id',$orderh->id)->where('Active',1)->sum('sellprice');
        $ttlbp=orderd::select('buyprice')->where('orderh_id',$orderh->id)->where('Active',1)->sum('buyprice');
        // dd($ttlsp);
        //$sellprice = number_format($sellprice);
         //dd($orderds);

        // foreach ($orderds as $orderd){
        //     $orderd = orderd::get();
        // }
        return view('Details.menu', compact('orderh','orderd','sellprice','ttlsp','ttlbp'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (auth()->user() ==null){
            return redirect()->route('login');
        } 
        return view('orderh.orderd.add');
    }

    public function createOrderD($id_order_h){
        if (auth()->user() ==null){
            return redirect()->route('login');
        } 
        $orderh = orderd::where('orderh_id',$id_order_h)->get();
        //dd($id_order_h);
        return view('Details.add', compact('id_order_h'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(Request $request)
    // {   
    //     if (auth()->user() ==null){
    //         return redirect()->route('login');
    //     } 

    //     $orderh = orderd::where('orderh_id',$orderh->id)->get();
    //     $orderd = orderd::create([
    //         'orderh_id' =>$orderh->id,
    //         'type' => $request->type,
    //         'itemname' => $request->name,
    //         'qty' => $request->qty,
    //         'sellprice' => $request->sellprice,
    //         'buyprice' => $request->buyprice,
    //         'commissionto' => $request->commissionto,
    //         'commission' => $request->commission,
    //         'note' => $request->note,
             
            
            
    //     ]);
        
    //     return redirect()->route('orderd.indexOrderD');
    // }

    public function storeOrderD(Request $request, $id_order_h)
    {   
        if (auth()->user() ==null){
            return redirect()->route('login');
        } 
        // dd($request->all());
        $orderh = orderd::where('orderh_id',$id_order_h)->get();

        $orderd = orderd::create([
            'orderh_id' => $id_order_h,
            'type' => $request->type,
            'itemname' => $request->itemname,
            'qty' => $request->quantity,
            'sellprice' => $request->sellprice,
            'buyprice' => $request->buyprice,
            'commissionto' => $request->commissionto,
            'commission' => $request->commission,
            'note' => $request->note
             
            
            
        ]);
        
        return redirect()->route('orderd.index.orderd', $id_order_h);

    }

    public function editOrderD($id_order_h, orderd $orderd )
    {
        //

        // $orderh = orderh::findOrFail($id_order_h);
        if (auth()->user() ==null){
             return redirect()->route('login');
        } 
        $id = $orderd->id;
        $type = $orderd->type;
        $itemname = $orderd->itemname;
        $qty = $orderd->qty;
        $sellprice = $orderd->sellprice;
        $buyprice = $orderd->buyprice;
        $commissionto = $orderd->commissionto;
        $commission = $orderd->commission;
        $note = $orderd->note;
        
        return view('Details.edit', compact('id_order_h','id','type','itemname','qty','sellprice','buyprice','commissionto','commission','note'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateOrderD(Request $request, $id_order_h, orderd $orderd)
    {
        // echo('edit');
        // $input = $request->all();
        // $orderh = orderh::findOrFail($id);
        // $orderh->fill($input)->save();
        // return redirect()->route('orderh.index')->with('success','Item edited successfully');
        if (auth()->user() ==null){
            return redirect()->route('login');
        } 
        // dd($request->all());
        $orderd->update($request->all());
        return redirect()->route('orderd.index.orderd', $id_order_h);

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyOrderD($id_order_h,$orderd )
    {
        //  echo('destroy');

        // $orderh = orderh::findOrFail($id);
        // orderh::destroy($orderh);
        if (auth()->user() ==null){
            return redirect()->route('login');
        } 
        // $orderd->delete();
        $Active = 0;
        $id = OrderD::findOrFail($orderd);
        // dd($id);
        $modBy = auth()->user()->name;
        // dd($modBy);
        $modAt = Carbon::now(new \DateTimeZone('Asia/Jakarta'));
        $id->Active = $Active;
        $id->delBy = $modBy;
        $id->delAt = $modAt;
        $id->save();
        // dd(DB::getQueryLog());
        //check if any foreign key
        //Session::flash('flash_message', 'Deck successfully deleted!');

        return redirect()->route('orderd.index.orderd', $id_order_h);
    }

    
}
