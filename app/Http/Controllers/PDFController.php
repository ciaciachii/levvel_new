<?php

namespace App\Http\Controllers;
use App\OrderH;
use App\Expenditure;
use PDF;
use DB;
use Illuminate\Http\Request;
use DateTime;

class PDFController extends Controller
{

	public function index()
    {
        //
        //$countprofits = 
        
            
        return view('Recap.recap');
    }
	public function pdf(Request $request){
//DD($request);
		$s = DateTime::createFromFormat('D d M Y', $request->start);
		$f = DateTime::createFromFormat('D d M Y', $request->finish);
		$st = date_format($s, 'Y-m-d');
		$fi = date_format($f, 'Y-m-d');
		$start = $st;
		$finish = $fi;
		$cia = $request->input('cia');
		$theo = $request->input('theo'); 
		$lodi = $request->input('lodi');

	    $orderh =OrderH::
	        select(
	            'orderh.id',
	            'orderh.name',
	            'orderh.date',
	            'orderh.paymentmethod',
	            DB::raw('(SUM(orderd.sellprice)-SUM(orderd.buyprice)-SUM(orderd.commission)) as profit')
	            )
	            ->leftjoin('orderd','orderh.id','=','orderd.orderh_id')
	            ->groupBy('orderh_id')
	            ->whereBetween('date', [$start, $finish])
                ->where('status',0)
                ->where('orderh.Active',1)
                ->where('orderd.Active',1)
	            ->get();
	    $orderhinc =OrderH::
	        select(
	            'orderh.id',
	            'orderh.name',
	            'orderh.date',
	            'orderh.paymentmethod',
	            DB::raw('(SUM(orderd.sellprice)-SUM(orderd.buyprice)-SUM(orderd.commission)) as profit')
	            )
	            ->leftjoin('orderd','orderh.id','=','orderd.orderh_id')
	            ->groupBy('orderh_id')
	            ->whereBetween('date', [$start, $finish])
                ->where('status',1)
                ->where('orderh.Active',1)
                ->where('orderd.Active',1)
	            ->get();
	            // dd($orderhinc);
	        $expenditure =Expenditure::
	        select(
	            'expenditure.id',
	            'expenditure.date',
	            'expenditure.name',
	            DB::raw('(SUM(amount)) as spend'),
		        'expenditure.addedby',
		        'expenditure.note'
	            )
	            ->groupBy('id')
	            ->whereBetween('date', [$start, $finish])
	            ->where('Active',1)
	            ->get();
	          
	    $commission=OrderH::
	        select(
	            'orderd.commissionto',
	            DB::raw('(SUM(commission)) as amount')
	            )
	            ->leftjoin('orderd','orderh.id','=','orderd.orderh_id')
	            ->groupBy('commissionto')
	            ->whereBetween('date', [$start, $finish])
	            ->where('orderh.Active',1)
                ->where('orderd.Active',1)
                ->where('status',0)
	            ->get();
	   
	    //$totals = DB::select(DB::raw($total));
	    $total = 'select sum(profit) as total from (select SUM(orderd.sellprice)-SUM(orderd.buyprice)-SUM(orderd.commission) as profit FROM `orderh` 
	    join `orderd` on `orderh`.`id` = `orderd`.`orderh_id`  
	    where orderh.date between "'.$start.'" and "'.$finish.'" 
	    AND  orderh.Active = "1"
	    AND  orderd.Active = "1"
	    group by `orderh_id` )tmp';

	    $totals = DB::select(DB::raw($total));

	    $exp = 'select sum(amount) as amt from expenditure where Active = "1" and date between "'.$start.'" and "'.$finish.'"' ;
	    $expe = DB::select(DB::raw($exp));
	    //dd($expe);

	    $ttlexp = 'select sum(profit)-(select SUM(expenditure.amount) from `expenditure` where date between "'.$start.'" and "'.$finish.'") as total from (select SUM(orderd.sellprice)-SUM(orderd.buyprice)-SUM(orderd.commission) as profit FROM `orderh` join `orderd` on `orderh`.`id` = `orderd`.`orderh_id`  where orderh.date between "'.$start.'" and "'.$finish.'" AND  orderh.Active = "1"
	    AND  orderd.Active = "1" group by `orderh_id` )tmp';
	    $ttlexpe = DB::select(DB::raw($ttlexp));
	    //dd($ttlexpe);

	    $pdf = PDF::loadView('Recap.pdf', compact ('orderh','expenditure','start','finish','totals','cia','theo','lodi','orderhinc','commission','ttlexpe','expe'));
	    //dd($pdf);
	    return $pdf->download('Levvel'.$start.'. - .'.$finish.'.pdf');
	    //return $pdf->stream();
	    //return view('pdf', compact ('orderh','start','finish'));
	}

}
