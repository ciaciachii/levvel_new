<?php

namespace App\Http\Controllers;
use DB;
use App\User;
use App\OrderH;
use App\Inventory;
use App\OrderD;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use App\Http\Requests;
use Auth;


//require 'vendor/autoload.php';

use Carbon\Carbon;
// use App\
use App\Todolist;
use App\Http\Requests\ListFormRequest;
use DateTime;

class InventoryCRUDController extends Controller
{
    public function index()
    {
        if (auth()->user() ==null){
            return redirect()->route('login');
        } 
        $inventory =Inventory::where('Active',1)->orderBy('id','desc')->get();
        
            
        return view('Inventories.menu', compact('inventory'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (auth()->user() ==null){
            return redirect()->route('login');
        } 
        return view('Inventories.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (auth()->user() ==null){
            return redirect()->route('login');
        } 
        $myDateTime = DateTime::createFromFormat('D d M Y', $request->date);
        $Active = 1;
        $inventory = Inventory::create([
            'id' => $request->id,
            'date' => $myDateTime,
            'name' => $request->name,
            'amount' => $request->amount,
            'addedby' => $request->addedby,
            'note' => $request->note
            
        ]);
        
        return redirect()->route('inventory.index');
    }

    public function edit($id)
    {
        //
        if (auth()->user() ==null){
            return redirect()->route('login');
        } 
        $inventory = Inventory::findOrFail($id);
       
       
        $date = $inventory->date;
        $name = $inventory->name;
        $amount = $inventory->amount;
        $addedby = $inventory->addedby;
        $note = $inventory->note;
        
        
        
        return view('Inventories.edit', compact('id','date','name','amount','addedby','note'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // echo('edit');
        // $input = $request->all();
        // $orderh = orderh::findOrFail($id);
        // $orderh->fill($input)->save();
        // return redirect()->route('orderh.index')->with('success','Item edited successfully');
        if (auth()->user() ==null){
            return redirect()->route('login');
        } 
        $input = $request->all();
         //dd($input);
         $ex = Inventory::findOrFail($id);
        // $orderh->fill($input)->save();
        // return redirect()->route('orderh.index')->with('success','Item edited successfully');

        $ex->name = $request->input('name');
        $ex->amount = $request->input('amount');
        $ex->addedby = $request->input('addedby');
        $ex->note = $request->input('note');
        //$save=orderh::find($id)->update($request->all());
        $ex->save();


        //Inventory::find($id)->update($request->all());
        return redirect()->route('inventory.index')
                        ;

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($inventory)
    {
        if (auth()->user() ==null){
            return redirect()->route('login');
        } 
        //  echo('destroy');

        // $orderh = orderh::findOrFail($id);
        // orderh::destroy($orderh);
        // $orderh->delete();
        // dd(DB::getQueryLog());
        //check if any foreign key
        //Session::flash('flash_message', 'Deck successfully deleted!');
        $Active = 0;
        $id = Inventory::findOrFail($inventory);
        // dd($id);
        $modBy = auth()->user()->name;
        // dd($modBy);
        $modAt = Carbon::now(new \DateTimeZone('Asia/Jakarta'));
        $id->Active = $Active;
        $id->delBy = $modBy;
        $id->delAt = $modAt;
        $id->save();
        return redirect()->route('inventory.index');
    }

    
}
