<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OrderH;
use App\Customer;
use Carbon\Carbon;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user() ==null){
            return redirect()->route('login');
        } 
        $now = Carbon::now(new \DateTimeZone('Asia/Jakarta'));
        // dd($now->month);
        // dd($now->year);
        // $get = OrderH::select('date')->get();

        $monthly = OrderH::select('date')->whereMonth('date','=',$now->month)->whereYear('date','=',$now->year)->where('Active',1)->count();
        // dd($month->date->month);
        // dd($month);
        $incomplete = OrderH::where('status',1)->where('Active',1)->count();
        //dd($incomplete);
        $complete = OrderH::where('status',0)->where('Active',1)->count();
        $customer = Customer::where('Active',1)->count();
        return view('home', compact('monthly','incomplete','complete','customer'));
    }
}
