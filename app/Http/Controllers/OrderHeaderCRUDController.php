<?php

namespace App\Http\Controllers;
use DB;
use App\User;
use App\OrderH;
use App\Customer;
use App\OrderD;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use App\Http\Requests;
use Auth;


//require 'vendor/autoload.php';

use Carbon\Carbon;
// use App\
use App\Todolist;
use App\Http\Requests\ListFormRequest;
use DateTime;

class OrderHeaderCRUDController extends Controller
{
    public function index()
    {
        //
        //$countprofits = 
        if (auth()->user() ==null){
            return redirect()->route('login');
        } 
        $orderh =OrderH::
            select(
                'orderh.id',
                'orderh.name',
                'customer.name as custname',
                'orderh.date',
                'orderh.addedby',
                'orderh.paymentmethod',
                'orderh.note',
                'orderh.status',
                'orderd.orderh_id'
                ,
                
                DB::raw('(SUM(orderd.sellprice)-SUM(orderd.buyprice)-SUM(orderd.commission)) as profit')
                )
             ->leftjoin('orderd','orderh.id','=','orderd.orderh_id')
             ->leftjoin('customer','customer.id','=','orderh.name')
             ->where('orderh.Active',1)
             ->groupBy('orderh_id')
             ->orderBy('orderh.id','desc')
             
             

             ->get();
             //dd($orderh);
        $date = OrderH::select('date')->get();

        //$date = strlen($date);
        //dd($date);
        // $orderh = DB::table('orderh')->get();
        //dd($date);
        //         $dt =  $orderh->date;
        //         //dd($dt);// 
        //          $m = $date->month;
        // //         // 
        //          $y = $date->year;
           // echo $date->format('m')->toString();  
            
        return view('Orders.menu', compact('orderh','date'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (auth()->user() ==null){
            return redirect()->route('login');
        } 
        $customer=Customer::get();
        // DD($customer);
        return view('Orders.add', compact('customer'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        if (auth()->user() ==null){
            return redirect()->route('login');
        } 
        $this->validate($request, [
                    'Customer' => 'required_without:name2',
                    'name2' => 'required_without:Customer',
                    'contact' => 'required_with:name2',
                    'date' => 'required',
                    'addedby' => 'required',
                    'paymentmethod' => 'required',
                    'status' => 'required'
                ]);
        $myDateTime = DateTime::createFromFormat('D d M Y', $request->date);
        // dd($request->all());
        // dd($myDateTime);
        if($request->name2!=null){
            
            $orderh = OrderH::create([
                        'name' => $request->name2,
                        'date' => $myDateTime,
                        'addedby' => $request->addedby,
                        'paymentmethod' => $request->paymentmethod,
                        'note' => $request->note,
                        'status' => $request->status 
            
            
                    ]);
            $cust = Customer::create([
                        'name' => $request->name2,
                        'contact' => $request->contact
                        
            
            
                    ]);
        }else{
            $orderh = OrderH::create([
                        'name' => $request->Customer,
                        'date' => $myDateTime,
                        'addedby' => $request->addedby,
                        'paymentmethod' => $request->paymentmethod,
                        'note' => $request->note,
                        'status' => $request->status 
                    ]);
        }
        
        return redirect()->route('orderh.index');
    }

    public function edit($id)
    {
        
        if (auth()->user() ==null){
            return redirect()->route('login');
        } 
        $orderh = OrderH::findOrFail($id);
        $customer=Customer::get();
        $name = $orderh->name;
        // dd($name);
        $custname = Customer::where('id',$name)->first();
        $date = date_format($orderh->date,'d-m-Y');
        // dd($date);
        $addedby = $orderh->addedby;
        $paymentmethod = $orderh->paymentmethod;
        $note = $orderh->note;
        $status = $orderh->status;
        
        
        
        return view('Orders.edit', compact('id','name','custname','date','addedby','paymentmethod','note','status','customer'));
    }

    public function update(Request $request, $id)
    {
        // echo('edit');
        if (auth()->user() ==null){
            return redirect()->route('login');
        } 
         $input = $request->all();
         // dd($request->input('date') != null);
         $orderh = OrderH::findOrFail($id);
         $date = $orderh->date;
         $date2 = date_format($date, 'd-m-Y');
         // dd($request->date);
         $myDateTime = DateTime::createFromFormat('d-m-Y', $request->date);
         // dd($myDateTime);
         $dateonly = date_format($myDateTime, 'd-m-Y');
         // dd($dateonly);
         // dd($dateonly === $date2);
        if($dateonly != $date2){
                     // dd($dateonly);

            $orderh = OrderH::findOrFail($id);
            // dd($myDateTime);
            $datee = date_format($myDateTime, 'Y-m-d');
            // dd($datee);
            // $orderh->fill($input)->save();
            // return redirect()->route('orderh.index')->with('success','Item edited successfully');
            // $orderh->name = $request->input('name');
            // $myDateTime = DateTime::createFromFormat('D d M Y', $request->date);
            $orderh->date = $datee;
            // dd($orderh->date);
            $orderh->addedby = $request->input('addedby');
            $orderh->paymentmethod = $request->input('paymentmethod');
            $orderh->note = $request->input('note');
            $orderh->status = $request->input('status');
            //$save=orderh::find($id)->update($request->all());
            $orderh->save();
        }
        else{
            $orderh = OrderH::findOrFail($id);
            // $orderh->fill($input)->save();
            // return redirect()->route('orderh.index')->with('success','Item edited successfully');
            // $orderh->name = $request->input('name');
            // $orderh->date = $myDateTime;
            $orderh->addedby = $request->input('addedby');
            $orderh->paymentmethod = $request->input('paymentmethod');
            $orderh->note = $request->input('note');
            $orderh->status = $request->input('status');
            //$save=orderh::find($id)->update($request->all());
            $orderh->save();
        }
            return redirect()->route('orderh.index')
                            ->with('success','Item updated successfully');
        

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($orderh)
    {
        if (auth()->user() ==null){
            return redirect()->route('login');
        } 
        //  echo('destroy');

        // $orderh = orderh::findOrFail($id);
        // orderh::destroy($orderh);
        // $orderh->delete();
        // dd(DB::getQueryLog());
        //check if any foreign key
        //Session::flash('flash_message', 'Deck successfully deleted!');
        $Active = 0;
        $id = OrderH::findOrFail($orderh);
        // dd($id);
        $modBy = auth()->user()->name;
        // dd($modBy);
        $modAt = Carbon::now(new \DateTimeZone('Asia/Jakarta'));
        $id->Active = $Active;
        $id->delBy = $modBy;
        $id->delAt = $modAt;
        $id->save();
            
        return redirect()->route('orderh.index')->with('success','Item deleted successfully');
    }

    
}
