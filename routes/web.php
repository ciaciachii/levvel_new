<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {

    return view('index');
})->name('login');

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/login', 'LoginController@login');
Route::post('/logout', 'LoginController@logout')->name('logout');

Route::resource('User/orderh', 'OrderHeaderCRUDController');
Route::resource('User/orderd', 'OrderDetailCRUDController');
Route::resource('User/expenditure', 'ExpenditureCRUDController');
Route::resource('User/inventory', 'InventoryCRUDController');
Route::resource('User/customer', 'CustomerCRUDController');

Route::get('User/orderd/create/{id}','OrderDetailCRUDController@createOrderD')->name('orderd.create.orderd');
Route::post('User/orderd/store/{id}','OrderDetailCRUDController@storeOrderD')->name('orderd.store.orderd');
Route::get('User/orderd/edit/{id_order_h}/{orderd}','OrderDetailCRUDController@editOrderD')->name('orderd.edit.orderd');
Route::patch('User/orderd/update/{id_order_h}/{orderd}','OrderDetailCRUDController@updateOrderD')->name('orderd.update.orderd');
Route::delete('User/orderd/destroy/{id_order_h}/{orderd}','OrderDetailCRUDController@destroyOrderD')->name('orderd.destroy.orderd');
Route::post('User/pdf','PDFController@pdf')->name('pdf');
Route::get('User/recap','PDFController@index')->name('recap');
Route::get('User/orderd/index/{id}','OrderDetailCRUDController@indexOrderD')->name('orderd.index.orderd');
Route::post('User/orderd/index/{id}','OrderDetailCRUDController@indexOrderD')->name('orderd.index.orderd');
